"use strict";

function createNewUser() {
  const _firstName = prompt("Enter your firstname");
  const _lastName = prompt("Enter your lastname");
  const newUser = {
    _firstName,
    _lastName,
    set firstName(value) {
      this._firstName = value;
    },
    get firstName() {
      return this._firstName;
    },
    set lastName(value) {
      this._lastName = value;
    },
    get lastName() {
      return this._lastName;
    },
    setFirstName(value) {
      this.firstName = value;
    },
    setLastName(value) {
      this.lastName = value;
    },
    getlogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
  };
  return newUser;
}

console.log(createNewUser().getlogin());
